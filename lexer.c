#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFSIZE 65535
#define TOKEN_SIZE 64

#define LINE_DELIM "\n"
#define TOKEN_DELIM " \t\r\a"

void init();
void getLines();
void tokenizeLines();

char *filename;


void tokenizeLines(char *line) {
	// parse/tokenize by word delimiters
    int bufsize = TOKEN_SIZE, position = 0;
    char *token, **tokens_backup;
    char **tokens = malloc(sizeof(char*) * bufsize);

    token = strtok(line, TOKEN_DELIM);

    while (token != NULL) {
        tokens[position] = token;
        position++;

        if (position >= bufsize) {
            printf("Expanding buffer size:\n");
            bufsize += TOKEN_SIZE;
            tokens_backup = tokens;
            tokens = realloc(tokens, sizeof(char*) * bufsize);
        }

        token = strtok(NULL, TOKEN_DELIM);
    }

    int result;

    // math operations
    // value return
    if (strncmp("+", tokens[1], sizeof(tokens[1])) == 0) {
	result = atoi(tokens[0]) + atoi(tokens[2]);
	printf("%d\n", result);
    }
    if (strncmp("-", tokens[1], sizeof(tokens[1])) == 0) {
	    result = atoi(tokens[0]) - atoi(tokens[2]);
	    printf("%d\n", result);
    }
    if (strncmp("*", tokens[1], sizeof(tokens[1])) == 0) {
	    result = atoi(tokens[0]) * atoi(tokens[2]);
	    printf("%d\n", result);
    }
    if (strncmp("/", tokens[1], sizeof(tokens[1])) == 0) {
	    result = atoi(tokens[0]) / atoi(tokens[2]);
	    printf("%d\n", result);
    }
    if (strncmp("%", tokens[1], sizeof(tokens[1])) == 0) {
	    result = atoi(tokens[0]) % atoi(tokens[2]);
	    printf("%d\n", result);
    }
    
	// value assignment
    if (strncmp("=", tokens[1], sizeof(tokens[1])) == 0) {
    	// unary operations	
    	if (sizeof(tokens) == 3) {
		result = atoi(tokens[2]);
	}
	// binary operations
    	if (strncmp("+", tokens[3], sizeof(tokens[3])) == 0) {
		result = atoi(tokens[2]) + atoi(tokens[4]);
	}
    	if (strncmp("-", tokens[3], sizeof(tokens[3])) == 0) {
		result = atoi(tokens[2]) - atoi(tokens[4]);
	}
    	if (strncmp("*", tokens[3], sizeof(tokens[3])) == 0) {
		result = atoi(tokens[2]) * atoi(tokens[4]);
	}
    	if (strncmp("/", tokens[3], sizeof(tokens[3])) == 0) {
		result = atoi(tokens[2]) / atoi(tokens[4]);
	}
    	if (strncmp("%", tokens[3], sizeof(tokens[3])) == 0) {
		result = atoi(tokens[2]) % atoi(tokens[4]);
	}
    }

	// increment
    if (strstr(tokens[0], "++") != NULL) {
	result = atoi(tokens[0]) + 1;
	printf("%d\n", result);
    }
	

    // how to store/recall data arbitrarily?
    // how to indicate that data being stored is still needed?
    // $result must be a temporary storage mechanism
    // long-term storage must be done in an array, and active storage
    // into those arrays must be monitored, and there must be links between
    // prior instructions and positions in the array
    // how to do that?

    if (strncmp("print", tokens[0], sizeof(tokens[0])) == 0) {
	    printf("%s\n", tokens[1]);
    }

    // comment handling
    if (strncmp("#", tokens[0], sizeof(tokens[0])) == 0) {
    	// ignore, do nothing
    }
}

void getLines(char *filebuf) {
	// parse/tokenize by line delimiters
    int bufsize = TOKEN_SIZE, position = 0;
    char *line, **lines_backup;
    char **lines = malloc(sizeof(char*) * bufsize);

    line = strtok(filebuf, LINE_DELIM);

    while (line != NULL) {
        lines[position] = line;
        position++;

        if (position >= bufsize) {
            bufsize += TOKEN_SIZE;
            lines_backup = lines;
            lines = realloc(lines, sizeof(char*) * bufsize);
        }

        line = strtok(NULL, LINE_DELIM);
    }

    position = 0;

    // null value being passed into tokenizeLines, how to avoid it
    while (lines[position] != NULL) {
        tokenizeLines(lines[position]);
        position++;
    }
}

void init(FILE* infile) {

    char c;
    int numLines = 0;
    char filebuf[BUFSIZE];

    // put file content into buffer
    if (infile != NULL) {
        while ((c = getc(infile)) != EOF) {
            strncat(filebuf, &c, sizeof(c));
	    if (c == '\n') {
	    	numLines++;	
	    }
        }
        fclose(infile);
	printf("Number of instructions: %d\n", numLines);
        getLines(filebuf);
    }
    else {
        printf("Please supply a file for the lexer to process.\n");
    }
}


int main(int argc, char* argv[]) {
    FILE* infile = fopen(argv[1], "r");
    filename = argv[1];
    init(infile);
    return 0;
}

KURIL

sandbox development for a lexer/parser, built in C (possibly some x86-asm inlining)
will post updates as I get new ideas/make progress

overall, this serves as me trying to understand how to build up 
a higher-level programming language, how to deal with persistent
data storage, referencing data, &c. all from scratch.

using a lot of x86 assembly as reference for storing and calling data

#include <stdio.h>

int main(void) {
	// push rbp
	// mov rbp,rsp
	// sub rsp,0x10 (allocate 16B for variables)	
	// beforehand, calculate the total size of the variables before allocating
	// how to determine the size beforehand?
	// determine size by dtype, how to determine dtype?

	// PRINTF is optimized as a puts()
	// lea rdi,[rbp+0x11b]
	// call 5a0 (puts@plt)
	printf("hello\n");

	// PRINTF is optimized as a putchar()
	// mov edi,0x35
	// call 590 (putchar@plt)
	printf("5");
	
	// MOVE VALUE CORRESPONDING TO $me into source index, and print
	// ACCORDING TO PROTOCOL 
	// mov esi,0x1 (2/2 = 1)
	// lea rdi,[rip+0x106] (put destAddr into dest index)
	// mov eax,0x0 [value for the write() syscall]
	// call 5b0 (printf@plt)	
	printf("%d\n", 2 / 2);


	// mov [rbp-0xc],0xa	
	int val = 3 + 7;
	// mov [rbp-0x8],0xffffffff
	int me = 11 - 12;

	// mov [rbp-0x4],0x1
	int thing = 1;
	// add [rbp-0x4],0x2
	thing += 2;
	// add [rbp-0x4],0x1
	thing++;

	
	// MOVE VALUE CORRESPONDING TO $me into source index, and print
	// ACCORDING TO PROTOCOL 
	// mov eax,[rbp-0x8] (put -1 into eax)
	// mov esi,eax (move -1 into source index)
	// lea rdi,[rip+0xd3] (put destAddr into dest index)
	// mov eax,0x0 [value for the write() syscall]
	// call 5b0 (printf@plt)	
	printf("%d\n", me);
	
	// MOVE VALUE CORRESPONDING TO $thing into source index, and print
	// ACCORDING TO PROTOCOL 
	// mov eax,[rbp-0x4] (put 4 into eax)
	// mov esi,eax (move 4 into source index)
	// lea rdi,[rip+0xbd] (put destAddr into dest index)
	// mov eax,0x0 [value for the write() syscall]
	// call 5b0 (printf@plt)	
	printf("%d\n", thing);
	
	// MOVE VALUE CORRESPONDING TO $me into source index, and print
	// ACCORDING TO PROTOCOL 
	// mov eax,[rbp-0x8] (put 10 into eax)
	// mov esi,eax (move 10 into source index)
	// lea rdi,[rip+0xa7] (put destAddr into dest index)
	// mov eax,0x0 [value for the write() syscall]
	// call 5b0 (printf@plt)	
	printf("%d\n", val);

	// MOVE RC0 into eax,then return
	// mov eax,0x0
	// leave
	// ret
	return 0;
}
